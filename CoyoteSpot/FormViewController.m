//
//  FormViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "FormViewController.h"
#import "DetailFormViewController.h"
@interface FormViewController ()

@end

@implementation FormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)btnCySightingClicked:(id)sender {
    [self pushController:1];
}

- (IBAction)btnCyEncounterClicked:(id)sender {
    [self pushController:2];
}

- (IBAction)btnCyUnattendedClicked:(id)sender {
    [self pushController:3];
}

- (IBAction)btnCyAttendedClicked:(id)sender {
    [self pushController:4];
}

-(void)pushController:(int)index{
    DetailFormViewController *detailformcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailFormViewController"];
    detailformcontroller.selectedIndex = index;
    [self.navigationController pushViewController:detailformcontroller animated:true];
}
@end
