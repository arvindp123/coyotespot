//
//  HomeViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "HomeViewController.h"
#import "MapViewController.h"
#import "SignUpViewController.h"
#import "SignInViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnContMapClicked:(id)sender {
    MapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    [self.navigationController pushViewController:map animated:true];
}
-(void)fetchData{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    [web getJsonResponse:@"" viewcontroller:self success:^(NSDictionary *response) {
        //success
    } failure:^(NSError *error) {
        //error
    }];
}
@end
