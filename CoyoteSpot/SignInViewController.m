//
//  SignInViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "SignInViewController.h"
#import "TxtfldTableViewCell.h"
#import "SignUpViewController.h"
#import "HomeViewController.h"

@interface SignInViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(nonatomic,strong)NSMutableArray *placehoder;
@property(nonatomic,strong)NSMutableArray *paramkey;
@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.placehoder = [[NSMutableArray alloc]initWithObjects:@"EmailId/Mobile",@"Password",nil];
      self.paramkey =[[NSMutableArray alloc]initWithObjects:@"username",@"password",nil];
    self.tblSigin.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tblSigin.bounds.size.width, 0.01f)];
    [self.navigationController.navigationBar setHidden:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableviewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.placehoder.count + 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"submitCell" forIndexPath:indexPath];
        UIButton *btn = [cell viewWithTag:1];
        [btn addTarget:self action:@selector(btnSubmitClicked:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
        TxtfldTableViewCell *txtcell = [tableView dequeueReusableCellWithIdentifier:@"txtfldCell" forIndexPath:indexPath];
        txtcell.txtfld.delegate = self;
        txtcell.txtfld.placeholder = self.placehoder[indexPath.row];
        return txtcell;
    }
}

#pragma mark - TextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return false;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 80;
}

- (IBAction)btnSignupClicked:(id)sender {
    SignUpViewController *signup = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self.navigationController pushViewController:signup animated:true];
}
-(void)btnSubmitClicked:(UIButton*)sender{
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    for(int i=0;i<self.placehoder.count;i++){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        TxtfldTableViewCell *cell = [self.tblSigin cellForRowAtIndexPath:indexPath];
        if ([cell.txtfld.text  isEqual: @""]){
            NSLog(@"empty");
            return;
        }
        paramdict[self.paramkey[i]] = cell.txtfld.text;
    }
    [self signinApi:paramdict];
}
-(void)signinApi:(NSMutableDictionary*)params{
    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:home animated:true];
    return; //testing
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    [web postJsonResponse:@"" params:params viewcontroller:self success:^(NSDictionary *response) {
        //success
        NSString* userid = response[@"user_id"];
        if (userid == nil || userid == (id)[NSNull null]) {
            // registeration failed
        } else {
            // success registeration done
            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:home animated:true];
        }
    } failure:^(NSError *error) {
        //error
    }];
}
@end
