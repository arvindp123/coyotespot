//
//  DetailFormViewController.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailFormViewController : UIViewController
@property(nonatomic,assign)int selectedIndex;
@property (weak, nonatomic) IBOutlet UIButton *btnTitle;
- (IBAction)btnBackClicked:(id)sender;
@end
