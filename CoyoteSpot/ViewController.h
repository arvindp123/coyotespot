//
//  MasterViewController.h
//  CoyoteSpot
//
//  Created by Shashi Dayal on 12/30/16.
//  Copyright © 2016 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MasterViewController : UIViewController {
    MKMapView *mapview;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@end

