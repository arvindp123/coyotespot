//
//  TxtfldTableViewCell.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TxtfldTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtfld;

@end
