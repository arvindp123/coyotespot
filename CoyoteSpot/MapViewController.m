//
//  MapViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "MapViewController.h"
#import "FormViewController.h"

@interface MapViewController ()<MKMapViewDelegate>

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnAlertClicked:(id)sender {
    FormViewController *formcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"FormViewController"];
    [self.navigationController pushViewController:formcontroller animated:true];
}
@end
