//
//  DetailFormViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "DetailFormViewController.h"

@interface DetailFormViewController ()

@end

@implementation DetailFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    switch(_selectedIndex){
        case 1 :
            [self.btnTitle setTitle:@"Coyote Sighting" forState:UIControlStateNormal];
            break; /* optional */
        case 2 :
             [self.btnTitle setTitle:@"Coyote Encounter" forState:UIControlStateNormal];
            break; /* optional */
        case 3 :
             [self.btnTitle setTitle:@"Coyote: Unattended Attack" forState:UIControlStateNormal];
            break; /* optional */
        case 4 :
             [self.btnTitle setTitle:@"Coyote: Attended Attack" forState:UIControlStateNormal];
            break; /* optional */
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];

}
@end
