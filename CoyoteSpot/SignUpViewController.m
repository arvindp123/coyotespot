//
//  SignUpViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "SignUpViewController.h"
#import "TxtfldTableViewCell.h"
#import "HomeViewController.h"

@interface SignUpViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(nonatomic,strong)NSMutableArray *placehoder;
@property(nonatomic,strong)NSMutableArray *paramkey;
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.placehoder = [[NSMutableArray alloc]initWithObjects:@"Firstname",@"Lastname",@"EmailId",@"Password",@"Mobile",@"Address",nil];
    self.paramkey =[[NSMutableArray alloc]initWithObjects:@"first_name",@"last_name",@"email",@"password",@"mobile_no",@"address",nil];
    self.tblSignup.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tblSignup.bounds.size.width, 0.01f)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.placehoder.count + 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == self.placehoder.count){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"submitCell" forIndexPath:indexPath];
        UIButton *btn = [cell viewWithTag:1];
        [btn addTarget:self action:@selector(btnSubmitClicked:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
      TxtfldTableViewCell *txtcell = [tableView dequeueReusableCellWithIdentifier:@"txtfldCell" forIndexPath:indexPath];
        txtcell.txtfld.delegate = self;
        txtcell.txtfld.placeholder = self.placehoder[indexPath.row];
        return txtcell;
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
  return false;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if ([textField.placeholder isEqual:@"Mobile"]){
    return newLength <= 10;
    }
    return newLength <= 80;
}

-(void)btnSubmitClicked:(UIButton*)sender{
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    for(int i=0;i<self.placehoder.count;i++){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        TxtfldTableViewCell *cell = [self.tblSignup cellForRowAtIndexPath:indexPath];
        if ([cell.txtfld.text  isEqual: @""]){
            NSLog(@"empty");
            return;
        }else if ([cell.txtfld.placeholder isEqual:@"EmailId"]){
            if([Utilities validateEmail:cell.txtfld.text] == false) {
                return;//  //Email Address is invalid.
            }
        }else if ([cell.txtfld.placeholder isEqual:@"Mobile"]){
             if([Utilities validatePhone:cell.txtfld.text] == false) {
                 return;//  //phone is invalid.
             }
        }
        paramdict[self.paramkey[i]] = cell.txtfld.text;
    }
    [self signupApi:paramdict];
}
-(void)signupApi:(NSMutableDictionary*)params{
    HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:home animated:true];
    return; //testing
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    [web postJsonResponse:@"" params:params viewcontroller:self success:^(NSDictionary *response) {
         //success
        NSString* userid = response[@"user_id"];
        if (userid == nil || userid == (id)[NSNull null]) {
            // registeration failed
        } else {
            // success registeration done
            HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:home animated:true];
        }
    } failure:^(NSError *error) {
        //error
    }];
}
@end
