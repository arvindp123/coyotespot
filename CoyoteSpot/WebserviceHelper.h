//
//  WebserviceHelper.h
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//


@interface WebserviceHelper : NSObject
-(void)getJsonResponse:(NSString *)urlStr viewcontroller:(UIViewController*)controller success:(void (^)(NSDictionary *response))success failure:(void(^)(NSError* error))failure;
-(void)postJsonResponse:(NSString *)urlStr params:(NSMutableDictionary*)parameter viewcontroller:(UIViewController*)controller success:(void (^)(NSDictionary *response))success failure:(void(^)(NSError* error))failure;

-(void)createSpinner:(UIViewController*)controller;
-(void)removeSpinner:(UIViewController*)controller;
-(void)checkNetwork:(UIViewController*)controller;
-(BOOL)connected;
@end
