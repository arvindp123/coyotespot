//
//  MapViewController.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface MapViewController : UIViewController{
     MKMapView *mapview;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)btnAlertClicked:(id)sender;
@end
